<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'controllers/RestController.php');

class User extends RestController {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('user_model');
    }

    private function okResponse($data) {
        return $this->response($data, self::HTTP_OK);
    }

    private function invalidResponse($data, $statusCode) {
        return $this->response($data, $statusCode);
    }  

    private function sendResponse($data, $statusCode = self::HTTP_BAD_REQUEST) {
        return $data['status'] ? $this->okResponse($data['data'] ?? []) : $this->invalidResponse($data, $statusCode);
    }

    private function validate($rules) {
        $this->form_validation->set_data($_GET);

        foreach ($rules as $key => $rule) {
            $this->form_validation->set_rules($key, $key, $rule);
        }

        if ($this->form_validation->run() == FALSE) {
            $this->sendResponse(['status' => false, 'message' => $this->form_validation->error_array()], 
                self::HTTP_UNPROCESSABLE_ENTITY);
            die;
        }

        return true;
    } 

    /**
     * user login
     * @author Hemaal Taras Hansda
     * @param username
     * @param password
     * @return [type] [description]
     */
    public function login_post() {
        $res = $this->user_model->login($this->post());
        $this->sendResponse($res);
    }

    /**
     * user register
     * @author Hemaal Taras Hansda
     * @param username
     * @param email
     * @param type
     * @param password
     * @param user_pref
     * @return [type] [description]
     */
    public function register_post() {
        $res = $this->user_model->register($this->post());
        $this->sendResponse($res);
    }

    /**
     * check user is logged in user or not
     * @author Hemaal Taras Hansda
     * @return [type] [description]
     */
    public function checkLogin_get() {
        $this->sendResponse([
            'status' => true,
            'data' => []
        ]);
    }

    /**
     * user logout
     * @author Hemaal Taras Hansda
     * @return [type] [description]
     */
    public function logout_delete() {
        $res = $this->user_model->logout($this->input->request_headers());
        $this->sendResponse($res);
    }
}

?>
