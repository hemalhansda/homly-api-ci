<?php defined('BASEPATH') OR exit('No direct script access allowed');


class User_model extends CI_Model {
    public $date = null;

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->date = date('Y-m-d H:i:s');
    }

    public function login($payload) {
        $username = $payload['username'] ?? '';
        $email = $payload['email'] ?? '';
        $password = $payload['password'] ?? '';

        $user_data = $this->db->query("SELECT
                ID,
                USERNAME,
                EMAIL,
                USER_PREF,
                USER_TYPE,
                PASSWORD,
                CREATED_AT,
                UPDATED_AT
            FROM
                USER
            WHERE (USERNAME = ?
                OR EMAIL = ?)", [
                $username, $email
            ])->row_array() ?? [];

        if (count($user_data) === 0) {
            return [
                'status' => false,
                'data' => [
                    "message" => "User not found!"
                ]
            ];
        }

        if (!password_verify($password, $user_data['PASSWORD'])) {
            return [
                'status' => false,
                'data' => [
                    "message" => "Incorrect password entered!"
                ]
            ];
        }

        $user_id = $user_data['ID'];

        $token = $this->generateJWTToken($user_id);
        $user_token_id = $this->insertUserToken($user_id, $token);
        $user_token = $this->getUserToken($user_id);

        $user_data['TOKEN'] = $user_token;
        unset($user_data['PASSWORD']);

        return [
            'status' => true,
            'data' => $user_data
        ];
    }

    public function register($payload) {
        $username = $payload['username'];
        $email = $payload['email'];
        $password = $payload['password'];
        $user_pref = $payload['user_pref'];
        $user_type = $payload['type'];

        $hash_options = [
            'cost' => 16,
        ];

        $password = password_hash($password, PASSWORD_BCRYPT, $hash_options); // for decrypting use password_verify

        $insertUpdate = $this->db->query("
            INSERT IGNORE INTO USER (USERNAME, EMAIL, PASSWORD, USER_PREF, USER_TYPE)
            VALUES (?, ?, ?, ?, ?) 
            ON DUPLICATE KEY UPDATE 
                USERNAME = VALUES (USERNAME),
                EMAIL = VALUES (EMAIL),
                PASSWORD = VALUES (PASSWORD),
                USER_PREF = VALUES (USER_PREF),
                USER_TYPE = VALUES (USER_TYPE)
        ", [$username, $email, $password, $user_pref, $user_type]);
        
        $insert_id = $this->db->insert_id();
        $user_id = $insert_id;

        $user_data = $this->getUserByID($user_id);

        $token = $this->generateJWTToken($user_id);

        $user_token_id = $this->insertUserToken($user_id, $token);

        $user_token = $this->getUserToken($user_id);

        $user_data['TOKEN'] = $user_token;
        
        return [
            'status' => true,
            'data' => $user_data
        ];
    }

    public function getUserToken($user_id) {
        $token = $this->db->query("SELECT
                TOKEN
            FROM
                USER_TOKENS
            WHERE
                USER_ID = ?
            LIMIT 1", [$user_id])->row_array()['TOKEN'];

        return $token;
    }

    public function insertUserToken($user_id, $token) {
        $this->db->query("INSERT INTO USER_TOKENS (USER_ID, TOKEN)
            VALUES (?, ?)", [$user_id, $token]);

        $user_token_id = $this->db->insert_id();

        return $user_token_id;
    }

    public function generateJWTToken($user_id) {
        $payload = json_encode(['user_id' => $user_id]);
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, config_item('salt'), true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }

    public function getUserByID($user_id) {
        $query = $this->db->query("SELECT
                ID,
                USERNAME,
                EMAIL,
                PASSWORD,
                USER_PREF,
                USER_TYPE,
                CREATED_AT,
                UPDATED_AT
            FROM
                USER
            WHERE
                ID = ?", [$user_id])->row_array();

        return $query;
    }

    public function logout($header) {
        $x_auth_token = $header['x-auth'] ?? '';

        $query = $this->db->query("DELETE FROM USER_TOKENS
            WHERE TOKEN = '$x_auth_token'");

        return [
            'status' => true,
            'data' => []
        ];
    }
}
